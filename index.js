/**
 * Created by xadrian on 27/10/17.
 */
require('dotenv').config()
var notificator = require('./src/notificator');
var db = require('./src/db');

exports.handler =  function handler(event, context, callback){
    db.connectDB().then(()=>{
        db.getAll().then(alerts=>{
            callback(null, alerts);
            
            let notifyPromises = []
            for(var pos in alerts){
                let alert = alerts[pos];
                let needNotify = notificator.needNotify(alert);
                if(needNotify){
                    notifyPromises.push(notificator.notify(alert))
                }
            }
                       
            Promise.all(notifyPromises).then(ok=>{
                db.close();
                callback(null,`${notifyPromises.length} alertas enviadas con exito`)
            }).catch(error=>{
                db.close();
                callback(error);
            })

            
        }).catch(error=>{
            db.close();
            callback(error);
        })
    }).catch(error=>{
        callback(error);
    })
}
