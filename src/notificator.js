/**
 * Created by xadrian on 19/05/17.
 */
var mail_notificator = require('./mail');
var slack_notificator = require('./slack');
var Promise = require('promise');


class notificator{
    notifySlack(channel, alertObject){
        return slack_notificator.send(channel,this.buildMessage(alertObject));
    } 
    notifyMail(mail, alertObject){
        let email_content = {TABLE:''};
        return mail_notificator.send(mail, this.buildMessage(alertObject), email_content);
    }
    needNotify(alertObject){
        let condition        = alertObject.condicion;
        let condition_object = JSON.parse(condition);
        let total            = alertObject.cuenta_total;
        let evals            = `${total} ${condition_object.operator} ${condition_object.total}`;
        let result = eval(evals);
        return result;
    }
    notify(alertObject){
        let self = this;
        return new Promise((resolve, reject)=>{
            let destinatarios = alertObject.destinatario.split(',');
            let notifys = [];
            for(let pos in destinatarios){
                let destinatario = destinatarios[pos];
                if(alertObject.pasarela == 'email'){
                    notifys.push(self.notifyMail(destinatario, alertObject));
                }else{
                    notifys.push(self.notifySlack(destinatario, alertObject));
                }
            }
            Promise.all(notifys).then(resolve).catch(reject);

        })
    }
    buildMessage(alertObject){
        let condition        = alertObject.condicion;
        let condition_object = JSON.parse(condition);
        let total            = alertObject.cuenta_total;
        return `La cuenta: *${alertObject.nombre}* tiene un total ${condition_object.operator} que ${condition_object.total}`;
    }

}
module.exports = new notificator();

