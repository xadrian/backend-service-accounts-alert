/**
 * Created by xadrian on 27/10/17.
 */
const SLACK_HOST = process.env.SLACK_HOST || 'https://hooks.slack.com/services/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';
const SLACK_USERNAME = process.env.SLACK_USERNAME || 'webhookbot';
const SLACK_EMOJI = process.env.SLACK_EMOJI || ':ghost:';

var Slack = require('slack-node');

class slack_notificator{    
    constructor(){
        this.slack = new Slack();
        this.slack.setWebhook(SLACK_HOST);
    }
    send(channel, message ){
        let self = this;
        return new Promise((resolve, reject)=>{
            self.slack.webhook({
                channel: channel,
                username: SLACK_USERNAME,
                icon_emoji: SLACK_EMOJI,
                text: message
            }, function(err, response) {
                if(err){
                    console.log("Ocurrio un error en el envio de notificacion",err);
                    return reject(err);
                }
                resolve(true);
            });
        })
    }
}
module.exports = new slack_notificator();