/**
 * Created by jose on 6/19/2017.
 */
var amqp = require('amqplib');
var Promise = require('promise');
const QUEUE_HOST = process.env.QUEUE_HOST || 'rbmq.autofact.qa';
const QUEUE_PORT = process.env.QUEUE_PORT || 5672;
const QUEUE_USER = process.env.QUEUE_USER || 'root';
const QUEUE_PASSWD = process.env.QUEUE_PASSWD || 'legaiamq';
const QUEUE_VHOST = process.env.QUEUE_VHOST || 'autofact';
const QUEUE_MAIL = process.env.QUEUE_MAIL || 'envio_email';

const ALERT_TEMPLATE = process.env.ALERT_TEMPLATE || 'notificacion_estado_folios';


class mail_notificator{
    sendToRabbit(message={}){
        return new Promise((resolve, reject)=>{
            amqp.connect('amqp://' + QUEUE_USER + ':' + QUEUE_PASSWD + '@' + QUEUE_HOST + ':' + QUEUE_PORT + '/' + QUEUE_VHOST).then(connection=> {
                console.log('Connection established');
                return connection.createChannel().then(channel => {
                    console.log('Channel connected');
                    const queueCheckOk = channel.assertQueue(QUEUE_MAIL);
                    return queueCheckOk.then(() => {
                        console.log('Queue asserted');
                        console.log('Sending message to queue');
                        channel.sendToQueue(QUEUE_MAIL, new Buffer(JSON.stringify(message)));
                        return channel.close();
                    });
                }).finally(() => {
                    console.log('Closing connection');
                    connection.close();
                });
            }).then(data=>{
                resolve(data);
            }).catch(error=>{
                console.log(error);
                return reject(error);
            });
        })
    }
    send(mail, title, email_contenido) {
        let self = this;
        var queue_message = {
            id: '',
            email: mail,
            vista: ALERT_TEMPLATE + '.php',
            subject: title,
            content: email_contenido
        };
        return self.sendToRabbit(queue_message);
    }
}
module.exports = new mail_notificator();