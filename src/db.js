﻿var mysql = require("mysql");
var Promise = require('promise');

var DB_HOST = process.env.DB_HOST || 'localhost';
var DB_PORT = process.env.DB_PORT || 3306;
var DB_USER = process.env.DB_USER || 'root';
var DB_PASS = process.env.DB_PASS || 'root';
var DB_DATABASE = process.env.DB_DATABASE || 'autofact';

var connection = null;
exports.connectDB = () => {
	return new Promise((resolve, reject) =>{
		connection = mysql.createConnection({
			host: DB_HOST,
			user: DB_USER,
			password: DB_PASS,
			database: DB_DATABASE,
			port: DB_PORT
		});
		
		connection.connect(err=> {
			if (err) {
				reject(err);
			    return;
			}
		    resolve(connection);
		});
	})
}
exports.getAll = () => new Promise((resolve, reject) => {
        connection.query("SELECT  ac.*, c.total as cuenta_total, c.nombre FROM alertas_cuenta ac inner join cuenta c on idcuenta = cuenta_idcuenta where activa = 1",
            (error, results, fields) => {
                if (error) {
                    console.log(error);
                    return reject(error);
                }
                resolve(results);
            });
    });
exports.close = () => new Promise((resolve, reject)=> {
	connection.end(err=> {
		if(err){
			return reject(err)
		}else{
			resolve(true);
		}
	});
})
